﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PATRON_MVC.Models
{

    ///CREACION DE CLASE "ALUMNO" LA CUAL SERA EL MODELO EN EL PROYECTO

    public class Alumno
    {
        // PROPIEDADES DE LA CLASE ALUMNO
        public int idAlumno { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }

        //"DataTime" => Representa un instante de tiempo
        public DateTime FecAlta { get; set; }
        public int Edad { get; set; }


    }
}