﻿using PATRON_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PATRON_MVC.Controllers
{
    public class AlumnosController : Controller
    {
        // GET: Alumnos
        public ActionResult Index()
        {
            // return View();


            //LLamado al Metodo RecuperaAlumnos

            var Alumnos = from a in RecuperaAlumnos()
                          orderby a.idAlumno          //Se ordenara los Alumnos por el ID
                          select a;                   //Mostrara toda la informacion

            return View(Alumnos);
            
        }

        // GET: Alumnos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Alumnos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Alumnos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Alumnos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Alumnos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Alumnos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Alumnos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //AGREGAMOS UN NUEVO METODO TIPO LISTA
        //La tarea de este metodo es que el controlador pida las peticiones a nuestra clase modelo
        //para luego desplegar la informacion en nuestra vista.


        [NonAction]    //[NonAction], quiere decir que no se podra pedir alguna peticion desde 
                       //             el navegador, es decir todo se manejara desde el controlador 
        

        public List<Alumno> RecuperaAlumnos()   //Hacemos referencia a nuestro modelo "Alumno"
        {
            return new List<Alumno>            
            {

                //Se Agrega el registro
                new Alumno
                {
                    idAlumno = 1,
                    Nombre  = "Daniel",
                    ApellidoPaterno = "Baque",
                    ApellidoMaterno = "Lopez",
                    FecAlta = DateTime.Parse(DateTime.Today.ToString()), // DateTime.Today.ToString()
                    Edad = 23,                                           // Solo para Demostracion.
                },
                new Alumno
                {
                    idAlumno = 2,
                    Nombre  = "Alexander",
                    ApellidoPaterno = "Garcia",
                    ApellidoMaterno = "Delgado",
                    FecAlta = DateTime.Parse(DateTime.Today.ToString()),
                    Edad = 22,
                },
                new Alumno
                {
                    idAlumno = 3,
                    Nombre  = "Alex",
                    ApellidoPaterno = "Barcia",
                    ApellidoMaterno = "Gil",
                    FecAlta = DateTime.Parse(DateTime.Today.ToString()),
                    Edad = 22,
                },
                new Alumno
                {
                    idAlumno = 4,
                    Nombre  = "Jose",
                    ApellidoPaterno = "Garcia",
                    ApellidoMaterno = "Lopez",
                    FecAlta = DateTime.Parse(DateTime.Today.ToString()),
                    Edad = 22,
                }
            };
        }
    }
}

